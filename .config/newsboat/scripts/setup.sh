#!/bin/sh
die() { printf "%s\n" "$*"; exit 1; }
command -v go > /dev/null || die "You must have go installed."
go install "github.com/ericchiang/pup@latest"

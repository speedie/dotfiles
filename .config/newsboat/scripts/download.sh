#!/bin/sh

YOUTUBE() {
    VIDEO_DL_DIR="$HOME/Videos"
    mkdir -p "$VIDEO_DL_DIR"
    PDIR="$(pwd)"
    cd "$VIDEO_DL_DIR"
    notify-send "📼Video downloading." "Downloading the video from '$@'"
    youtube-dl -f b --prefer-ffmpeg "$@" > /dev/null
    cd "$PDIR"
    notify-send "📼Video downloaded." "Video downloaded and saved to '$VIDEO_DL_DIR'."
    exit 0
}

echo "$@" | grep -qE "yewtu|youtube" && YOUTUBE "$@" &

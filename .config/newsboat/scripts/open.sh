#!/bin/sh

echo "$@" | grep -qE "yewtu|piped.video|youtube.com" && PREFIX="mpv"

[ -z "$PREFIX" ] && PREFIX="xdg-open"

command -v "$PREFIX" > /dev/null || exit 1
command -v notify-send > /dev/null && [ "$PREFIX" = "mpv" ] && \
    notify-send "🌐 Playing video" "🌐 Playing video from https://youtube.com in mpv"

command -v notify-send > /dev/null && [ "$PREFIX" = "xdg-open" ] && \
    notify-send "🌐 Opening link" "🌐 Opening link in your web browser"

$PREFIX "$@"

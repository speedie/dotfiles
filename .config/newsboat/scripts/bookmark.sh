#!/bin/sh
FILE="$HOME/.config/newsboat/bookmarks"

touch "$FILE"
[ "$#" -eq 0 ] && exit 1
command -v curl && url=$(curl -sIL -o /dev/null -w '%{url_effective}' "$1") || url="$1"

# perl expr to parse it
url=$(echo "${url}" | perl -p -e 's/(\?|\&)?utm_[a-z]+=[^\&]+//g;' -e 's/(#|\&)?utm_[a-z]+=[^\&]+//g;')
title="$2"
description="$3"

grep -q "${url}\t${title}\t${description}" "$FILE" || printf "%s\t%s\t%s" "${url}" "${title}" "${description}" >> "$FILE"

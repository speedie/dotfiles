#!/bin/sh
# music conky script
#
# settings
enable_filename=1 # enable filename (0/1)
enable_artist=1 # enable artist (0/1)
enable_title=1 # enable title (0/1)
enable_album=1 # enable album (0/1)
enable_genre=1 # enable genre (0/1)
enable_time_elapsed=1 # enable time elapsed (0/1)
enable_time_total=1 # enable time total (0/1)

# format
#
# @a - artist
# @t - title
# @g - genre
# @ab - album
# @tt - total time
# @te - time elapsed
# @fn - filename
# @cfn - cut filename (no file extension)
# @dn - dirname
# @p - path to song
#
# example: "@a - @t from @ab (@g) [@te/@tt]"
line1_format="@a - @t"
line2_format="from @ab (@g) [@te/@tt]"

cover_art_file="/tmp/cover.jpg"

############################################################

# check music player
check_player() {
    client="cmus"
    pidof mocp >/dev/null && client=mocp
    pidof cmus >/dev/null && client=cmus

    pidof $client > /dev/null || exit 1

    return
}

# get properties
getprop() {
    [ -z "$client" ] && exit 1

    get_cmus_data() {
		    [ "$enable_filename" = "1" ]     && music_filename="$(basename "$(cmus-remote -C status | grep file)")"
		    [ "$enable_filename" = "1" ]     && music_filename_cut="$(basename "$(cmus-remote -C status | grep file)"| sed 's|\(.*\)[.].*|\1|')"
		    [ "$enable_filename" = "1" ]     && music_filedir="$(dirname "$(cmus-remote -C status | grep file | sed 's/file //g')")"
            [ "$enable_artist" = "1" ]       && music_artist="$(cmus-remote -C status | grep "tag artist" | sed "s/tag artist //g")"
      		[ "$enable_title" = "1" ]        && music_title="$(cmus-remote -C status | grep "tag title" | sed "s/tag title //g")"
     		[ "$enable_album" = "1" ]        && music_album="$(cmus-remote -C status | grep "tag album " | head -n 1 | sed "s/tag album //g")"
		    [ "$enable_genre" = "1" ]        && music_genre="$(cmus-remote -C status | grep "tag genre" | sed "s/tag genre //g")"
		    [ "$enable_time_elapsed" = "1" ] && music_timeelapsed="$(cmus-remote -Q | grep position | sed "s/position //g" | awk '{printf "%02d:%02d:%02d",$0/3600,$0%3600/60,$0%60}' | sed "s/00://")"
		    [ "$enable_time_total" = "1" ]   && music_timetotal="$(cmus-remote -Q | grep duration | sed "s/duration //g" | awk '{printf "%02d:%02d:%02d",$0/3600,$0%3600/60,$0%60}' | sed "s/00://")"
    }

    # for mocp
    get_mocp_data() {
		    [ "$enable_filename" = "1" ]     && music_filename="$(basename "$(mocp -Q %file)")"
		    [ "$enable_filename" = "1" ]     && music_filename_cut="$(basename "$(mocp -Q %file)" | sed 's|\(.*\)[.].*|\1|')"
		    [ "$enable_filename" = "1" ]     && music_filedir="$(dirname "$(mocp -Q %file)")"
            [ "$enable_artist" = "1" ]       && music_artist="$(mocp -Q %artist)"
     		[ "$enable_title" = "1" ]        && music_title="$(mocp -Q %song)"
      		[ "$enable_album" = "1" ]        && music_album="$(mocp -Q %album)"
		    [ "$enable_time_elapsed" = "1" ] && music_timeelapsed="$(mocp -Q %ct)"
		    [ "$enable_time_total" = "1" ]   && music_timetotal="$(mocp -Q %tt)"
		    # todo: genre
    }

    case "$client" in
        "cmus") get_cmus_data ;;
        "mocp") get_mocp_data ;;
    esac

    return
}

# get art with ffmpeg and other factors
gen_cover_art() {
    command -v ffmpeg > /dev/null || exit 1
    getprop
    rm -f "$cover_art_file"
    hc=0
    [ ! -e "${music_filedir}/${music_filename}" ] && exit 1
    [ -e "${music_filedir}/${music_filename}.cover.jpg" ] && cp "${music_filedir}/${music_filename}.cover.jpg" "$cover_art_file" && hc=1
    [ -e "${music_filedir}/cover.jpg" ] && cp "${music_filedir}/cover.jpg" "$cover_art_file" && hc=1
    [ "$hc" = "0" ] && ffmpeg -i "${music_filedir}/${music_filename}" -map 0:1 "$cover_art_file" -loglevel quiet
    exit 0
}

# generate timebar
gen_timebar() {
    [ -z "$client" ] && exit 1

    get_cmus_bar() {
        dur=$( cmus-remote -Q | grep "duration" | cut -c10- )
        pos=$( cmus-remote -Q | grep "position" | cut -c10- )
        echo "($pos/$dur)*100" | bc -l
    }

    case "$client" in
        "cmus") get_cmus_bar ;;
    esac

    exit 0
}

# print metadata for line 1
print_metadata_1() {
    getprop
    data="$(printf "%s$line1_format" | sed "s|@ab|$music_album|; s|@a|$music_artist|; s|@g|$music_genre|; s|@tt|$music_timetotal|; s|@te|$music_timeelapsed|; s|@t|$music_title|; s|@fn|$music_filename|; s|@cfn|$music_filename_cut|; s|@dn|$music_filedir|; s|@g|$music_genre|; s|@p|$music_filedir/$music_filename|g")" || return
    printf "%s\n" "$data"
    exit 0
}

# print metadata for line 2
print_metadata_2() {
    getprop
    data="$(printf "%s$line2_format" | sed "s|@ab|$music_album|; s|@a|$music_artist|; s|@g|$music_genre|; s|@tt|$music_timetotal|; s|@te|$music_timeelapsed|; s|@t|$music_title|; s|@fn|$music_filename|; s|@cfn|$music_filename_cut|; s|@dn|$music_filedir|; s|@g|$music_genre|; s|@p|$music_filedir/$music_filename|g")" || return
    printf "%s\n" "$data"
    exit 0
}

# main function
main() {
    check_player

    case "$1" in
        "-gen_cover_art") gen_cover_art ;;
        "-gen_timebar") gen_timebar ;;
        "-p1") print_metadata_1 ;;
        "-p2") print_metadata_2 ;;
    esac

    exit 0
}

main "$@"

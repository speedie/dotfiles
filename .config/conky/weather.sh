#!/bin/sh

DATA1="$(curl -s wttr.in/?format="%C")"
DATA2="$(curl -s wttr.in/?format=3 | sed 's/.* //; s/.*\(.....\)/\1/')"

# if no internet connection/it failed to download
[ -z "$DATA1" ] && printf "No internet connection.\n" > /tmp/module_weather_log && exit 1

DATA1="$DATA1, $DATA2"

printf "%s\n" "$DATA1"

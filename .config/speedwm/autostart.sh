#!/bin/sh
# spDE / speedwm autostart.sh
#
# Feel free to add/remove from this.
CONFDIR="${XDG_CONFIG_HOME:-$HOME/.config}"

LOC="${LOC:-59.32938:18.06871}"

[ -f "$CONFDIR/.Xresources" ] && command -v xrdb > /dev/null && xrdb -override "$CONFDIR/.Xresources"
[ -e "$CONFDIR/speedwm/swal/swal_wm" ] && "$CONFDIR/speedwm/swal/swal_wm"

command -v libspeedwm > /dev/null && pidof speedwm > /dev/null && libspeedwm --perform core_wm_reload
command -v xmodmap > /dev/null && [ -f "$CONFDIR/.Xmodmap" ] && xmodmap "$CONFDIR/.Xmodmap"
command -v picom > /dev/null && picom &
command -v pasystray > /dev/null && pasystray &
command -v blueman-applet > /dev/null && blueman-applet &
command -v redshift-gtk > /dev/null && redshift-gtk -l "${LOC}" &
command -v conky > /dev/null && conky &
command -v xmousepasteblock > /dev/null && xmousepasteblock &
command -v dunst > /dev/null && dunst &
command -v dnoted > /dev/null && dnoted &

#!/bin/sh
# togglewindow.sh
# Simple script for hiding/showing windows (with history) similar to awesome
# License: GNU General Public License version 3.0
[ ! -x "$(command -v xdotool)" ] && printf "xdotool not found, install it.\n" && exit 1
case "$1" in
    "--hide")
        window="$(xdotool getactivewindow)" || exit 1
        [ -z "$window" ] && exit 1
        printf "%s\n" "$window" >> /tmp/activewindow
        xdotool windowunmap "$(tail -n 1 /tmp/activewindow)"
        exit 0
    ;;
    "--show")
        [ ! -f "/tmp/activewindow" ] || [ -z "$(tail -n 1 /tmp/activewindow)" ] && exit 1
        xdotool windowmap "$(tail -n 1 /tmp/activewindow)"
        sed -i '$d' /tmp/activewindow
        exit 0
    ;;
    "--clear")
        rm -f "/tmp/activewindow"; exit 0
    ;;
    *)
cat << EOF
usage: $0 [--hide] [--show] [--clear]
EOF
    ;;
esac

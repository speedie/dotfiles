# speedie's hyprland config
# Copy to ~/.config/hypr/hyprland.conf

monitor=,preferred,auto,auto

exec-once = hyprpaper & waybar & ~/.config/hypr/autostart.sh

env = XCURSOR_SIZE,24

input {
    kb_layout = us
    kb_variant =
    kb_model =
    kb_options =
    kb_rules =

    follow_mouse = 1

    touchpad {
        natural_scroll = no
    }

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

general {
    layout = master

    gaps_in = 5
    gaps_out = 5
    border_size = 1
    no_border_on_floating = true
    col.active_border = rgba(ffffffee) rgba(00ff99ee) 45deg
    col.inactive_border = rgba(595959aa)
}

misc {
    layers_hog_keyboard_focus = true
    focus_on_activate = true
    disable_hyprland_logo = true
    disable_splash_rendering = true
    mouse_move_enables_dpms = true
    enable_swallow = true
    swallow_regex = ^(st)$
}

decoration {
    rounding = 0
    blur = no
    drop_shadow = no
    active_opacity = 1.0
    inactive_opacity = 0.8
}

animations {
    enabled = no
}

master {
    new_is_master = false
}

windowrule = float, spmenu
windowrule = float, file_progress
windowrule = float, confirm
windowrule = float, dialog
windowrule = float, download
windowrule = float, notification
windowrule = float, error
windowrule = float, splash
windowrule = float, confirmreset
windowrule = float, title:Open File
windowrule = float, title:branchdialog
windowrule = idleinhibit focus, mpv
windowrule = idleinhibit fullscreen, firefox
windowrule = float, title:^(Media viewer)$
windowrule = float, title:^(Volume Control)$
windowrule = float, title:^(Picture-in-Picture)$
windowrulev2 = tile,class:^(Chromium)$

binde=, XF86AudioRaiseVolume, exec, speedwm-audioctrl -raise
binde=, XF86AudioLowerVolume, exec, speedwm-audioctrl -lower
binde=, XF86AudioMute, exec, speedwm-audioctrl -mute
bind=, XF86AudioPlay, exec, playerctl play-pause
bind=, XF86AudioPause, exec, playerctl play-pause
bind=, XF86AudioNext, exec, playerctl next
bind=, XF86AudioPrev, exec, playerctl previous

binde = SUPER CTRL, Q, exec, speedwm-audioctrl -mute
binde = SUPER CTRL, W, exec, speedwm-audioctrl -lower
binde = SUPER CTRL, E, exec, speedwm-audioctrl -raise

bind = SUPER SHIFT, Return, exec, st
bind = SUPER SHIFT, W, exec, chromium
binde = SUPER SHIFT, Q, killactive,
bind = SUPER SHIFT, F, exec, st -e vifmrun
bind = SUPER SHIFT, M, exec, musique
bind = SUPER SHIFT, Space, togglefloating,
bind = SUPER SHIFT, semicolon, exec, spmenu_run -x
bind = SUPER, semicolon, exec, spmenu_run -d
bind = SUPER CTRL, S, exec, speedwm-screenshotutil -f
bind = SUPER SHIFT, S, exec, speedwm-screenshotutil -s

bind = SUPER CTRL SHIFT, B, exec, speedwm-btctrl
bind = SUPER CTRL SHIFT, N, exec, speedwm-netctrl

binde = SUPER, O, exec, ~/.config/hypr/togglewindow.sh --hide
binde = SUPER SHIFT, O, exec, ~/.config/hypr/togglewindow.sh --show

binde = SUPER SHIFT, H, resizeactive, -50 0
binde = SUPER SHIFT, L, resizeactive, 50 0
binde = SUPER SHIFT, K, resizeactive, 0 -20
binde = SUPER SHIFT, J, resizeactive, 0 20

bind = SUPER, F, fullscreen,

binde = SUPER, H, movefocus, l
binde = SUPER, L, movefocus, r
binde = SUPER, K, movefocus, u
binde = SUPER, J, movefocus, d

binde = SUPER CTRL, H, movewindow, l
binde = SUPER CTRL, L, movewindow, r
binde = SUPER CTRL, K, movewindow, u
binde = SUPER CTRL, J, movewindow, d

bind = SUPER, 1, workspace, 1
bind = SUPER, 2, workspace, 2
bind = SUPER, 3, workspace, 3
bind = SUPER, 4, workspace, 4
bind = SUPER, 5, workspace, 5
bind = SUPER, 6, workspace, 6
bind = SUPER, 7, workspace, 7
bind = SUPER, 8, workspace, 8
bind = SUPER, 9, workspace, 9
bind = SUPER, 0, workspace, 10

bind = SUPER SHIFT, 1, movetoworkspace, 1
bind = SUPER SHIFT, 2, movetoworkspace, 2
bind = SUPER SHIFT, 3, movetoworkspace, 3
bind = SUPER SHIFT, 4, movetoworkspace, 4
bind = SUPER SHIFT, 5, movetoworkspace, 5
bind = SUPER SHIFT, 6, movetoworkspace, 6
bind = SUPER SHIFT, 7, movetoworkspace, 7
bind = SUPER SHIFT, 8, movetoworkspace, 8
bind = SUPER SHIFT, 9, movetoworkspace, 9
bind = SUPER SHIFT, 0, movetoworkspace, 10

bind = SUPER, mouse_down, workspace, e+1
bind = SUPER, mouse_up, workspace, e-1

bindm = SUPER, mouse:272, movewindow
bindm = SUPER, mouse:273, resizewindow

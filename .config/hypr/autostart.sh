#!/bin/sh
command -v pasystray > /dev/null && pasystray &
command -v xmousepasteblock > /dev/null && xmousepasteblock &
command -v dunst > /dev/null && dunst &
command -v dnoted > /dev/null && dnoted &
command -v nheko > /dev/null && nheko &
command -v clipmenud > /dev/null && clipmenud &
export CM_LAUNCHER="spmenu -g 1"

/* speedie's .Xresources file.
 *
 * To use with Alacritty: https://gist.github.com/zacharied/e33300ce340caa70499d516ed8ac8de6
 */

/* Color scheme to use
 * - SOLARIZED_DARK
 * - SOLARIZED_LIGHT
 * - NORD
 * - DRACULA
 * - GRUVBOX_DARK
 * - GRUVBOX_LIGHT
 *
 * selected colorscheme
 * example: #define NORD
 */
#define NORD

/* Misc */
#define SPEEDWM /* Enable speedwm options */
#define ST /* Enable st options */
#define DMENU /* Enable dmenu options */

#define ENABLE_SPEEDWM_COLORS /* Enable speedwm colors */
#define ENABLE_ST_COLORS /* Enable st colors */
#define ENABLE_DMENU_COLORS /* Enable dmenu colors */

#define ENABLE_COLORS /* Enable color options */
#define ENABLE_GLOBAL_COLORS /* Enable global colors */

//#define ENABLE_SPEEDWM_OPTIONS /* Enable speedwm options */
#define ENABLE_ST_OPTIONS /* Enable st options */
#define ENABLE_DMENU_OPTIONS /* Enable dmenu options */

/* nord */
#ifdef NORD
    #define col_0  #2E3440
    #define col_1  #3B4252
    #define col_2  #434C5E
    #define col_3  #4C566A
    #define col_4  #D8DEE9
    #define col_5  #E5E9F0
    #define col_6  #ECEFF4
    #define col_7  #8FBCBB
    #define col_8  #88C0D0
    #define col_9  #81A1C1
    #define col_10 #5E81AC
    #define col_11 #BF616A
    #define col_12 #D08770
    #define col_13 #EBCB8B
    #define col_14 #A3BE8C
    #define col_15 #B48EAD
#endif

/* solarized dark */
#ifdef SOLARIZED_DARK
    #define col_0  #002b36
    #define col_1  #073642
    #define col_2  #586e75
    #define col_3  #657b83
    #define col_4  #839496
    #define col_5  #93a1a1
    #define col_6  #eee8d5
    #define col_7  #fdf6e3
    #define col_8  #dc322f
    #define col_9  #cb4b16
    #define col_10 #b58900
    #define col_11 #859900
    #define col_12 #2aa198
    #define col_13 #268bd2
    #define col_14 #6c71c4
    #define col_15 #d33682
#endif

/* solarized light */
#ifdef SOLARIZED_LIGHT
    #define col_0  #fdf6e3
    #define col_1  #eee8d5
    #define col_2  #93a1a1
    #define col_3  #839496
    #define col_4  #657b83
    #define col_5  #586e75
    #define col_6  #073642
    #define col_7  #002b36
    #define col_8  #dc322f
    #define col_9  #cb4b16
    #define col_10 #b58900
    #define col_11 #859900
    #define col_12 #2aa198
    #define col_13 #268bd2
    #define col_14 #6c71c4
    #define col_15 #d33682
#endif

/* dracula */
#ifdef DRACULA
    #define col_0  #282936
    #define col_1  #3a3c4e
    #define col_2  #4d4f68
    #define col_3  #626483
    #define col_4  #62d6e8
    #define col_5  #e9e9f4
    #define col_6  #f1f2f8
    #define col_7  #f7f7fb
    #define col_8  #ea51b2
    #define col_9  #b45bcf
    #define col_10 #00f769
    #define col_11 #ebff87
    #define col_12 #a1efe4
    #define col_13 #62d6e8
    #define col_14 #b45bcf
    #define col_15 #00f769
#endif

/* gruvbox (dark) */
#ifdef GRUVBOX_DARK
    #define col_0   #1d2021
    #define col_1   #3c3836
    #define col_2   #504945
    #define col_3   #665c54
    #define col_4   #bdae93
    #define col_5   #d5c4a1
    #define col_6   #ebdbb2
    #define col_7   #fbf1c7
    #define col_8   #fb4934
    #define col_9   #fe8019
    #define col_10  #fabd2f
    #define col_11  #b8bb26
    #define col_12  #8ec07c
    #define col_13  #83a598
    #define col_14  #d3869b
    #define col_15  #d65d0e
#endif

/* gruvbox (light) */
#ifdef GRUVBOX_LIGHT
    #define col_0  #f9f5d7
    #define col_1  #ebdbb2
    #define col_2  #d5c4a1
    #define col_3  #bdae93
    #define col_4  #665c54
    #define col_5  #504945
    #define col_6  #3c3836
    #define col_7  #282828
    #define col_8  #9d0006
    #define col_9  #af3a03
    #define col_10 #b57614
    #define col_11 #79740e
    #define col_12 #427b58
    #define col_13 #076678
    #define col_14 #8f3f71
    #define col_15 #d65d0e
#endif

/* speedwm */
#ifdef SPEEDWM
    #ifdef ENABLE_SPEEDWM_OPTIONS
        speedwm.bar.height:                    3 ! Height of the bar in pixels (<num>)
        speedwm.bar.position:                  1 ! Position of the bar (0:  Bottom, 1:  Top)
        speedwm.bar.paddingoh:                 10 ! Horizontal padding (extra space) around the bar in pixels (<num>)
        speedwm.bar.paddingov:                 10 ! Vertical padding (extra space) around the bar in pixels (<num>)
        speedwm.bar.paddingih:                 0 ! Horizontal padding (extra space) inside the bar in pixels (<num>)
        speedwm.bar.paddingiv:                 0 ! Vertical padding (extra space) inside the bar in pixels (<num>)
        speedwm.bar.hide:                      0 ! Hide the bar by default (0/1)
        speedwm.bar.hide.tags:                 0 ! Hide the tags (0/1)
        speedwm.bar.hide.emptytags:            1 ! Hide the tags that have no windows open (0/1)
        speedwm.bar.hide.floating:             0 ! Hide the floating window indicator (0/1)
        speedwm.bar.hide.layout:               0 ! Hide the layout indicator (0/1)
        speedwm.bar.hide.sticky:               0 ! Hide the sticky indicator (0/1)
        speedwm.bar.hide.status:               0 ! Hide the status bar (0/1)
        speedwm.bar.hide.systray:              0 ! Hide the systray (0/1)
        speedwm.bar.hide.unselected.title:     0 ! Hide the unselected window title (0/1)
        speedwm.bar.hide.title:                0 ! Hide the window title (0/1)
        speedwm.bar.hide.icon:                 0 ! Hide the window icon (0/1)
        speedwm.bar.hide.clientindicator:      0 ! Hide the client indicator on the tags (0/1)
        speedwm.bar.titleposition:             1 ! Position of the title (0:  Left, 1:  Center)
        speedwm.border.roundedcorners:         0 ! Enable rounded corners around the border of windows (0/1)
        speedwm.border.cornerradius:           2 ! Corner radius when rounded corners are enabled (<num>)
        speedwm.border.size:                   1 ! Size of the border around windows in pixels (<num>)
        speedwm.client.allowurgent:            1 ! Allow windows to have the 'urgent' status (0/1)
        speedwm.client.automove:               1 ! Allow windows to move themselves on demand. This may be annoying depending on what software you use and is disabled by default for this reason (0/1)
        speedwm.client.autofocus:              1 ! Allow windows to focus themselves on demand. This may be annoying depending on what software you use and is disabled by default for this reason (0/1)
        speedwm.client.autoresize:             1 ! Allow windows to resize themselves on demand (0/1)
        speedwm.client.decorhints:             1 ! Respect decoration hints for windows (0/1)
        speedwm.client.hide.border:            0 ! Hide all window borders for windows (0/1)
        speedwm.client.hide.unselected.border: 1 ! Hide all window borders for windows (0/1)
        speedwm.client.hide.single.border:     1 ! Hide the window border when only a single window is visible (0/1)
        speedwm.client.fade.inactive:          1 ! Fade inactive windows (windows that are not focused) (0/1)
        speedwm.client.fade.windows:           1 ! Fade windows (0/1)
        speedwm.client.floatscratchpad:        0 ! Float a scratchpad window when it is shown (0/1)
        speedwm.client.savefloat:              1 ! Save the position of floating windows when they are no longer floating (0/1)
        speedwm.client.swallow:                1 ! Allow clients to be swallowed by the terminal emulator (0/1)
        speedwm.client.swallowfloating:        1 ! Also allow floating clients to be swallowed (0/1)
        speedwm.client.wmclass:                1 ! Enable workaround for when a class cannot be grabbed from a client (0/1)
        speedwm.fonts.font:                    NotoSans Regular 9
        speedwm.text.tag1.empty:               
        speedwm.text.tag2.empty:               
        speedwm.text.tag3.empty:               
        speedwm.text.tag4.empty:               
        speedwm.text.tag5.empty:               
        speedwm.text.tag6.empty:               
        speedwm.text.tag7.empty:               
        speedwm.text.tag8.empty:               
        speedwm.text.tag9.empty:               
        speedwm.text.tag1.used:                
        speedwm.text.tag2.used:                
        speedwm.text.tag3.used:                
        speedwm.text.tag4.used:                
        speedwm.text.tag5.used:                
        speedwm.text.tag6.used:                
        speedwm.text.tag7.used:                
        speedwm.text.tag8.used:                
        speedwm.text.tag9.used:                
        speedwm.text.layout1:                  L1
        speedwm.text.layout2:                  L2
        speedwm.text.layout3:                  L3
        speedwm.text.layout4:                  L4
        speedwm.text.layout5:                  L5
        speedwm.text.layout6:                  L6
        speedwm.text.layout7:                  L7
        speedwm.text.layout8:                  L8
        speedwm.text.layout9:                  L9
        speedwm.text.layout10:                 L10
        speedwm.text.layout11:                 L11
        speedwm.text.layout12:                 L12
        speedwm.text.layout13:                 L13
        speedwm.text.layout14:                 L14
        speedwm.text.layout15:                 L15
        speedwm.color.hiddentitle:             1 ! Color the hidden title (0/1)
        speedwm.color.layout:                  0 ! Color the layout indicator (0/1)
        speedwm.color.selectedtitle:           0 ! Color the selected title (0/1)
        speedwm.bar.powerline.tag.shape:       0 ! Shape of the tag powerlines (0:  >, 1:  /)
        speedwm.focus.spawned:                 1 ! Focus the newly spawned window or keep focus (0/1)
        speedwm.fullscreen.hidebar:            1 ! Hide the bar when a client is fullscreened (0/1)
        speedwm.fullscreen.lockfullscreen:     1 ! Lock the fullscreen (0/1)
        speedwm.fullscreen.movefullscreenmon:  0 ! Allow moving fullscreened windows to another monitor (0/1)
        speedwm.gaps.enable:                   1 ! Enable gaps around the windows
        speedwm.gaps.sizeih:                   10 ! Horizontal inner gap size (<num>)
        speedwm.gaps.sizeiv:                   10 ! Vertical inner gap size (<num>)
        speedwm.gaps.sizeoh:                   10 ! Horizontal outer gap size (<num>)
        speedwm.gaps.sizeov:                   10 ! Vertical outer gap size (<num>)
        speedwm.gaps.smartgaps:                0 ! Enable different gap size when only one client is spawned (0/1)
        speedwm.gaps.smartgapsize:             0 ! Size of the gaps when only one client is spawned (0/1)
        speedwm.icon.size:                     10 ! Size of the window icon in the taskbar (<num>)
        speedwm.icon.spacing:                  5 ! Spacing between icon and text in the taskbar (<num>)
        speedwm.layout.deck.count:             0 ! Enable deck count in the deck layout (0/1)
        speedwm.layout.deck.format:            [%d]
        speedwm.layout.monocle.clientcount:    0 ! Enable client count in the monocle layout (0/1)
        speedwm.layout.monocle.count:          0 ! Enable focused client and number of total clients in the monocle layout (0/1)
        speedwm.layout.monocle.format:         [%d/%d]
        speedwm.mfact:                         0.50 ! Default mfact (0-1)
        speedwm.mfact.lowest:                  0.05 ! Lowest possible mfact (0-1)
        speedwm.mouse.clicktofocus:            0 ! Require clicking on a window to focus or focus when the cursor touches it (0/1)
        speedwm.mouse.mfact: 		           1 ! Enable adjusting mfact with your mouse (0/1)
        speedwm.mouse.cfact:                   1 ! Enable adjusting cfact with your mouse (0/1)
        speedwm.rule.refresh:                  0 ! Allow rules to be refreshed if a title/class changes (0/1)
        speedwm.stack.attachdirection:         3 ! Direction to attach windows in (0:  Default, 1:  Above, 2:  Aside, 3:  Below, 4:  Bottom, 5:  Top)
        speedwm.stack.centerfloating:          1 ! Center floating windows (0/1)
        speedwm.stack.i3mcount:                0 ! Enable i3 like nmaster/mastercount (0/1)
        speedwm.stack.mastercount:             1 ! Number of master clients in the master stack to start with (0/1)
        speedwm.stack.snap:                    20 ! Snap pixel (<char>)
        speedwm.status.defaultstatus:
        speedwm.switcher.maxheight:            200 ! Max height of the switcher in pixels (<char>)
        speedwm.switcher.maxwidth:             600 ! Max width of the switcher in pixels (<char>)
        speedwm.switcher.menupositionv:        1 ! Menu position of the switcher vertically (0:  Bottom, 1:  Center, 2:  Top)
        speedwm.switcher.menupositionh:        1 ! Menu position of the switcher horizontally (0:  Left, 1:  Center, 2:  Right)
        speedwm.systray.padding:               2 ! Extra padding between status and systray (0/1)
        speedwm.systray.pinning:               0 ! Always show systray on the focused monitor or <num> (0/<monitor number>)
        speedwm.systray.position:              0 ! Position of the systray horizontally (0:  Right, 1:  Left)
        speedwm.tag.pertag:                    1 ! Allow tags to have their own values (0/1)
        speedwm.tag.preview:                   1 ! Enable tag previews (0/1)
        speedwm.tag.preview.bar:               1 ! Include the bar in the tag preview (0/1)
        speedwm.tag.preview.paddingh:          0 ! Extra horizontal padding between the tag preview and the edge of the screen (<num>)
        speedwm.tag.preview.paddingv:          0 ! Extra vertical padding between the tag preview and the bar (<num>)
        speedwm.tag.preview.scale:             4 ! Amount to scale down the preview picture by (<num>)
        speedwm.tag.resetgaps:                 0 ! Reset gaps when the tag has no clients (0/1)
        speedwm.tag.resetlayout:               0 ! Reset layout when the tag has no clients (0/1)
        speedwm.tag.resetmfact:                0 ! Reset mfact when the tag has no clients (0/1)
        speedwm.tag.start:                     1 ! Start on a tag on startup (0/1)
        speedwm.tag.underline:                 0 ! Draw an underline on the tags (0/1)
        speedwm.tag.underlineall:              0 ! Show underline on all tags or just the selected tags (0/1)
        speedwm.tag.underlinepad:              5 ! Horizontal padding between the underline and tag (<num>)
        speedwm.tag.underlinestroke:           2 ! Height of the underline in pixels (<num>)
        speedwm.tag.underlinevoffset:          0 ! How far above the bottom of the bar the line should appear (<num)
        speedwm.tag.urgentwindows:             1 ! Color tags that have urgent tags on them (0/1)
        speedwm.tiling.resizehints:            0 ! Enable resize hints (0/1)
        speedwm.run.shell:                     /bin/sh
        /*
        speedwm.status.hideemptymodule:        1
        speedwm.status.leftpadding:
        speedwm.status.rightpadding:
        speedwm.status.separator:
        */
    #endif

    #ifdef ENABLE_COLORS
    #ifdef ENABLE_SPEEDWM_COLORS
        speedwm.col.layout:                    #99b3ff
        speedwm.col.layouttext:                #000000
        speedwm.col.status0:                   #131210
        speedwm.col.status1:                   #bf616a
        speedwm.col.status2:                   #A16F9D
        speedwm.col.status3:                   #68ABAA
        speedwm.col.status4:                   #A89F93
        speedwm.col.status5:                   #D3A99B
        speedwm.col.status6:                   #AFC9AC
        speedwm.col.status7:                   #eae1cb
        speedwm.col.status8:                   #a39d8e
        speedwm.col.status9:                   #6D5E8E
        speedwm.col.status10:                  #a16f9d
        speedwm.col.status11:                  #d3a99b
        speedwm.col.status12:                  #afc9ac
        speedwm.col.status13:                  #eae1cb
        speedwm.col.status14:                  #6d5e8e
        speedwm.col.status15:                  #ffffff
        speedwm.col.powerline0:                #131210
        speedwm.col.powerline1:                #bf616a
        speedwm.col.powerline2:                #A16F9D
        speedwm.col.powerline3:                #68ABAA
        speedwm.col.powerline4:                #A89F93
        speedwm.col.powerline5:                #D3A99B
        speedwm.col.powerline6:                #AFC9AC
        speedwm.col.powerline7:                #eae1cb
        speedwm.col.powerline0_text:           #eeeeee
        speedwm.col.powerline1_text:           #131210
        speedwm.col.powerline2_text:           #131210
        speedwm.col.powerline3_text:           #131210
        speedwm.col.powerline4_text:           #131210
        speedwm.col.powerline5_text:           #131210
        speedwm.col.powerline6_text:           #131210
        speedwm.col.powerline7_text:           #131210
        speedwm.col.systray:                   #222222
        speedwm.col.tag1:                      #99b3ff
        speedwm.col.tag2:                      #99b3ff
        speedwm.col.tag3:                      #99b3ff
        speedwm.col.tag4:                      #99b3ff
        speedwm.col.tag5:                      #99b3ff
        speedwm.col.tag6:                      #99b3ff
        speedwm.col.tag7:                      #99b3ff
        speedwm.col.tag8:                      #99b3ff
        speedwm.col.tag9:                      #99b3ff
        speedwm.col.tag1.text:                 #eeeeee
        speedwm.col.tag2.text:                 #eeeeee
        speedwm.col.tag3.text:                 #eeeeee
        speedwm.col.tag4.text:                 #eeeeee
        speedwm.col.tag5.text:                 #eeeeee
        speedwm.col.tag6.text:                 #eeeeee
        speedwm.col.tag7.text:                 #eeeeee
        speedwm.col.tag8.text:                 #eeeeee
        speedwm.col.tag9.text:                 #eeeeee
        speedwm.col.tagurgent:                 #f0e68c
        speedwm.col.background:                #222222
        speedwm.col.textnorm:                  #bbbbbb
        speedwm.col.textsel:                   #222222
        speedwm.col.titlenorm:                 #222222
        speedwm.col.titlesel:                  #99b3ff
        speedwm.col.titlehid:                  #222222
        speedwm.col.windowbordernorm:          #000000
        speedwm.col.windowbordersel:           #eeeeee
        speedwm.col.windowborderurg:           #f0e68c
    #endif
    #endif
#endif

/* st */
#ifdef ST
    #ifdef ENABLE_ST_COLORS
        st.color0:          col_1
        st.color1:          col_11
        st.color2:          col_14
        st.color3:          col_13
        st.color4:          col_9
        st.color5:          col_15
        st.color6:          col_8
        st.color7:          col_5
        st.color8:          col_3
        st.color9:          col_11
        st.color10:         col_14
        st.color11:         col_13
        st.color12:         col_9
        st.color13:         col_15
        st.color14:         col_7
        st.color15:         col_6
        st.foreground:      col_4
        st.background:      col_0
        st.cursorColor:     col_4
        st.curcolor:        col_4
    #endif
    #ifdef ENABLE_ST_OPTIONS
        st.termname:        st-256color
        st.shell:           /bin/sh
        st.understyle:      1
        st.urlcmd:          xdg-open
        st.gradient:        0
        st.graddirection:   0
        st.minlatency:      8
        st.maxlatency:      33
        st.synctimeout:     200
        st.blinktimeout:    800
        st.bellvolume:      0
        st.tabspaces:       4
        st.borderpx:        0
        st.boxdraw:         1
        st.boxdraw_bold:    1
        st.boxdraw_braille: 0
        st.defaultfg:       257
        st.defaultbg:       256
        st.defaultcs:       258
        st.defaultrc:       259
        st.cursorstyle:     1
        st.brightbold:      0
        st.alpha:           0.8
        st.gradalpha:       0.54
        st.statalpha:       0.46
        st.cwscale:         1.0
        st.chscale:         1.0
        st.cxoffset:        0
        st.cyoffset:        0
    #endif
#endif

/* dmenu */
#ifdef DMENU
    #ifdef ENABLE_DMENU_COLORS
        dmenu.color0:           col_1
        dmenu.color1:           col_11
        dmenu.color2:           col_14
        dmenu.color3:           col_13
        dmenu.color4:           col_9
        dmenu.color5:           col_15
        dmenu.color6:           col_8
        dmenu.color7:           col_5
        dmenu.color8:           col_3
        dmenu.color9:           col_11
        dmenu.color10:          col_14
        dmenu.color11:          col_13
        dmenu.color12:          col_9
        dmenu.color13:          col_15
        dmenu.color14:          col_7
        dmenu.color15:          col_6
    #endif
    #ifdef ENABLE_DMENU_OPTIONS
        dmenu.font:             DejaVu Sans Mono 8
        dmenu.menuposition:     1
        dmenu.menupaddingv:     0
        dmenu.menupaddingh:     0
        dmenu.bordercentered:   1
        dmenu.borderwidth:      2
        dmenu.lines:            0
        dmenu.columns:          10
        dmenu.lineheight:       20
        dmenu.minlineheight:    5
        dmenu.maxhist:          64
        dmenu.histnodup:        1
    #endif
#endif

/* apply schemes */
#ifdef ENABLE_COLORS
#ifdef ENABLE_GLOBAL_COLORS
    *.foreground:   col_4
    *.background:   col_0
    *.cursorColor:  col_4
    *.color0:       col_1
    *.color1:       col_11
    *.color2:       col_14
    *.color3:       col_13
    *.color4:       col_9
    *.color5:       col_15
    *.color6:       col_8
    *.color7:       col_5
    *.color8:       col_3
    *.color9:       col_11
    *.color10:      col_14
    *.color11:      col_13
    *.color12:      col_9
    *.color13:      col_15
    *.color14:      col_7
    *.color15:      col_6
#endif
#endif
